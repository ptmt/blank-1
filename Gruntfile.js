module.exports = function(grunt) {
    var dev = grunt.option('dev'),
        noimg = grunt.option('noimg'),
        project_banner = '/*! <%= pkg.name %> v<%= pkg.version %> <%= grunt.template.today("yyyy-mm-dd") %> @romanyanke*/\n';

    var grunt_config = {
        pkg: grunt.file.readJSON('package.json'),

        connect: {
            server: {
                options: {
                    port: 3000,
                    base: './build',
                    keepalive: 0,
                    livereload: true,
                    hostname: '*'
                }
            }
        },

        watch: {
            options: {
                livereload: true
            },
            scriptMain: {
                files: ['src/script/main/**/*.js', 'src/script/main.js'],
                tasks: ['uglify']
            },
            scriptLib: {
                files: ['src/script/lib/**/*.js'],
                tasks: ['sync:scriptLib']
            },
            views: {
                files: ['src/**/*.jade'],
                tasks: ['jade']
            },
            style: {
                files: ['src/style/**/*.styl'],
                tasks: ['stylus']
            },
            img: {
                files: ['src/images/**.*', '!src/images/sprite/**.*'],
                tasks: ['sync']
            },
            spritesheet: {
                files: ['src/images/sprite/**.*'],
                tasks: ['sprite', 'stylus']
            }
        },

        jade: {
            compile: {
                options: {
                    pretty: true,
                    data: {
                        project_title: '<%= pkg.name %> <%= pkg.version %>',
                        welcome: 'hi there!'
                    }
                },
                files:
                [
                {
                    expand: true,
                    cwd: 'src',
                    src: ['**/*.jade', '!includes/*.jade', '!layout.jade'],
                    dest: 'build/',
                    ext: '.html'
                }
                ]
            }
        },

        stylus: {
            compile: {
                options: {
                    banner: project_banner,
                    import: [
                    'lib/normalize.css',
                    'nib',
                    'sprite'
                    ],
                    'include css': true
                },
                files: {
                    'build/css/main.css': 'src/style/main.styl',
                }
            }
        },

        uglify: {
            options: {
                banner: project_banner
            },
            build: {
                src: ['src/script/main/**/*.js', 'src/script/main.js'],
                dest: 'build/js/main.js'
            }
        },

        concat: {
            options: {
                separator: ';\n'
            },
            main: {
                src: ['src/script/main/**/*.js', 'src/script/main.js'],
                dest: 'build/js/main.js'
            }
        },

        imagemin: {
            static: {
                options: {
                    optimizationLevel: 3
                },
            },
            all: {
                files: [{
                    expand: true,
                    cwd: 'build/img/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'build/img/'
                }]
            }
        },

        copy: {
            modernizr: {
                src: 'src/script/modernizr.min.js',
                dest: 'build/js/modernizr.min.js',
            }
        },

        sprite: {
            spritesheet: {
                src: 'src/images/sprite/*.png',
                destImg: 'build/img/spritesheet.png',
                destCSS: 'src/style/sprite.styl',
                imgPath: '../img/spritesheet.png'
            }
        },

        sync: {
            images: {
                files: [{
                    expand: true,
                    cwd: 'src/images/',
                    src: ['**/*{png,jpg,gif}', '!sprite/**/*'],
                    dest: 'build/img/',
                }]
            },
            scriptLib: {
                files: [{
                    expand: true,
                    flatten: true,
                    cwd: 'src/script/lib/',
                    src: ['**/*.js'],
                    dest: 'build/js/',
                }]
            }
        }
    };

    if (dev) {
        grunt_config.watch.scriptMain.tasks = ['concat'];
    }

    grunt.initConfig(grunt_config);
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-jade');
    grunt.loadNpmTasks('grunt-contrib-stylus');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-spritesmith');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-sync');

    var tasks = ['connect', 'jade', 'sync'];

    if (dev) {
        tasks.push('concat');
    } else {
        tasks.push('uglify');
    }

    if (!noimg) {
        tasks = tasks.concat(['sprite', 'imagemin']);
    }

    tasks = tasks.concat(['stylus', 'watch']);

    grunt.registerTask('default', tasks);
    grunt.registerTask('img', ['sprite', 'sync', 'imagemin']);

};